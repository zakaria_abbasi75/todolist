<?php
session_start();
include "constants.php";
include BASE_PATH. "bootstarp/config.php";
include BASE_PATH. "vendor/autoload.php";
include BASE_PATH. "libs/helpers.php";

$dsn = "mysql:dbname=$database_config->db;host=$database_config->host";
$user = "$database_config->user";
$password = "$database_config->pass";
try{    
    $pdo = new PDO($dsn, $user, $password); 
}catch (PDOException $e){
    diePage('Connection Faild :   '.$e->getMessage());
}
// echo "Connection to Database is Ok!";

include BASE_PATH. "libs/libs-auth.php";
include BASE_PATH. "libs/libs-tasks.php";