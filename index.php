<?php
error_reporting(E_ALL);
ini_set('display_errors' ,1);
include "bootstarp/init.php";

if (isset($_GET['logout'])) {
    logOut();
 }

if (!isLoggedIn()) {
   
   redirect(siteUrl('auth.php'));
}

#user is logged in
$user =getLoggedInUser();


if(isset($_GET['delete_folder']) && is_numeric($_GET['delete_folder'])){
    $deletedCount  = deleteFolder($_GET['delete_folder']);
    //echo "$deletedCount folder successfuly deleted!"; 
}
if(isset($_GET['delete_task']) && is_numeric($_GET['delete_task'])){
    $deletedCount  = deleteTasks($_GET['delete_task']);
    //echo "$deletedCount task successfuly deleted!"; 
}
 


 $folders = getFolders();
 
 $tasks = getTasks();
//  dd($tasks);

require_once "tpl/tpl-index.php";

?>