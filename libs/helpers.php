<?php
defined('BASE_PATH') OR  
die("<div style='background-color:#f3ecec; margin:50px auto;width: 50%; padding: 25px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>Permision Denied</div>");

function getCurrentUrl()   
{
    return 1;
}

function isAjaxRequest()
{
    if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' ) {
        return true;
    }
    return false;
}
function siteUrl($uri = '')
{
    return BASE_URL .$uri;
    
}
function redirect($url)
{
    header("location:".$url);
    die();
}
function diePage($msg) 
{
    echo "<div style='background-color:#f3ecec; margin:50px auto;width: 50%; padding: 25px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>$msg</div>";
    die();
}
function message($msg ,$cssClass ='info') 
{
    echo "<div class='$cssClass' style='background-color:#f3ecec; margin:10px auto;width:40%; padding: 10px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>$msg</div>";
    
}
function dd($var)
{
    echo '<pre style="background-color: #fff; z-index: 999; position: relative; margin: 10px; padding: 10px; border-radius: 10px; border-left: 5px solid #dc1729; color: #ab4545;">';
    var_dump($var);
    echo "</pre>";
}
