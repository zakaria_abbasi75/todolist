<?php
defined('BASE_PATH') OR  
die("<div style='background-color:#f3ecec; margin:50px auto;width: 50%; padding: 25px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>Permision Denied</div>");

    /*****Auth Function *****/

    #get login user id
    function getCurrentUuserIid ()
    {
        return getLoggedInUser()->id ?? 0;
    }
    function isLoggedIn()   
    {
        
        return isset($_SESSION['login'])? true :false;
    }
    function getLoggedInUser()   
    {
        
        return $_SESSION['login'] ?? null ;
    }
    function getUserByEmail($email){
        global $pdo;
              
        $sql     = "SELECT * FROM tbl_users 
        WHERE email = :email ";
        $stmt    = $pdo ->prepare($sql);
        $stmt    ->execute([':email'=>$email]);
        $record  = $stmt->fetchAll(PDO::FETCH_OBJ);
        return $record[0] ?? null;

    } 
    function logOut(){
        unset($_SESSION['login']);
        

    }   
    function login($email , $pass)   
    {
        $user = getUserByEmail($email);
        if (is_null($user)) {
            return false;
        }
        #check the password
        if (password_verify($pass , $user->password)) {
            #Login is successfull
            $user->image  = "https://www.gravatar.com/avatar/" . md5( strtolower( trim( $user->email ) ) ) ;
            $_SESSION['login'] = $user; 
            
            return true;  
        }
        return false;
    }
    

    function register($userData)
    {
        global $pdo;
        $pass =password_hash( $userData['password'] , PASSWORD_ARGON2I);
        $name = $userData['name'];
        $email = $userData['email'];
        
            $sql = "INSERT INTO tbl_users
             (name,email,password )
             VALUES (:name ,:email , :pass)";
            $stmt = $pdo->prepare($sql);
            $stmt->bindParam(":name", $name);
            $stmt->bindParam(":email", $email);
            $stmt->bindParam(":pass", $pass);
            $stmt->execute();
            return $stmt->rowCount()? true : false;
        
    }

   