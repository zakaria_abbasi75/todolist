<?php

// if (!defined('BASE_PATH')) {
//     echo "<div style='background-color:#f3ecec; margin:50px auto;width: 50%; padding: 25px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>Permision Denied</div>";
//     die();
// }

defined('BASE_PATH') OR  
   die("<div style='background-color:#f3ecec; margin:50px auto;width: 50%; padding: 25px; border-radius: 10px; border: 1px solid red; color: red; text-align: center; font-size: 16px;'>Permision Denied</div>");


/*****Folders Function *****/

function deleteFolder($folder_id)
{
    global $pdo;
    $sql = "DELETE FROM tbl_folders WHERE id = $folder_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount();   
}

function addFolder($folder_name)
{
    global $pdo;
    $current_user_id = getCurrentUuserIid();
    $sql = "INSERT INTO tbl_folders
    (user_id, name )
     VALUES (:id, :name)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":id" , $current_user_id);
    $stmt->bindParam(":name" , $folder_name);
    $stmt->execute();
    return $stmt->rowCount();   
}

function getFolders()
{
    global $pdo;
    $current_user_id = getCurrentUuserIid();
    $sql     = "SELECT * FROM tbl_folders 
    WHERE user_id =$current_user_id ORDER BY id DESC";
    $stmt    = $pdo ->prepare($sql);
    $stmt    ->execute();
    $record  = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $record;
}



/*****Tasks Function *****/
function deleteTasks($task_id)
{
    global $pdo;
    $sql = "DELETE FROM tbl_tasks WHERE id = $task_id";
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount(); 
}
function doneSwitch($task_id)
{
    global $pdo;
    $current_user_id = getCurrentUuserIid();
    $sql = "UPDATE tbl_tasks SET is_done = 1- is_done
    WHERE user_id =:userId AND  id = :isDone;";
    $stmt = $pdo->prepare($sql);
    $stmt->execute([":userId"=>$current_user_id ,":isDone" =>$task_id]);
    return $stmt->rowCount();  
}
function addTasks($task_title ,$folder_id)
{
    global $pdo;
    $current_user_id = getCurrentUuserIid();
    $sql = "INSERT INTO tbl_tasks
    (title,user_id,folder_id)
     VALUES (:title ,:userId, :folderId)";
    $stmt = $pdo->prepare($sql);
    $stmt->bindParam(":title" , $task_title);
    $stmt->bindParam(":userId" , $current_user_id);
    $stmt->bindParam(":folderId" , $folder_id);
    $stmt->execute();
    return $stmt->rowCount();
}
function getTasks()
{
    global $pdo;
    $folder = $_GET['folder_id'] ?? null;
    $folderCondition = '';
    if (isset($folder) AND is_numeric($folder)) {
        $folderCondition = "ANd folder_id = $folder";
    }
    if (isset($folder) AND is_numeric($folder)) {
        $folderCondition = "ANd folder_id = $folder";
    }
    $current_user_id = getCurrentUuserIid();
    $sql     = "SELECT * FROM tbl_tasks 
    WHERE user_id =$current_user_id  $folderCondition ORDER BY id DESC";
    $stmt    = $pdo ->prepare($sql);
    $stmt    ->execute();
    $record  = $stmt->fetchAll(PDO::FETCH_OBJ);
    return $record;
}