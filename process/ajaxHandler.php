<?php

include_once "../bootstarp/init.php";
error_reporting(E_ALL);
ini_set('display_errors', 1);
if (!isAjaxRequest()) {
    diePage("Invalid Request!");
}
if (!isset($_POST['action']) || empty($_POST['action'])){
    diePage("Invalid Action!");
}
switch($_POST['action']){
   

    case"doneSwitch":
        $task_id= $_POST['taskId'];
        if (!isset($task_id) || !is_numeric($task_id)) {
            echo "ایدی تسک معتبر نمی باشد!";
            die();
        }
        echo doneSwitch($task_id);
        break;

    case "addFolder":
        if (!isset($_POST['folderName']) || strlen($_POST['folderName']) < 3 ) {
            echo "نام فولدر باید بزرگتر از ۲ حرف باشد";
            die();
        }
        echo addFolder($_POST['folderName']);
         break;

    case "addTask":
        $taskTitle = $_POST['taskTitle'];
        $folderId  = $_POST['folderId'];
        
        if (!isset($folderId) || empty($folderId)) {
            echo "لطفا فولدری را انتخاب کنید!";
            die();
        }
        if(!isset($taskTitle) || strlen($taskTitle) < 3 ) {
            echo "عنوان تسک باید بزرگتر از ۲ حرف باشد";
            die();
        }
        echo addTasks($taskTitle, $folderId );
        break;
    
    default:
        diePage("Invalid Action!");
}

