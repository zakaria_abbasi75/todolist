
<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title><?= SITE_TITLE ?></title>
  <link rel="stylesheet" href="<?= BASE_URL?>assets/css/style.css">

</head>
<body>
<!-- partial:index.partial.html -->
<div class="page">
  <div class="pageHeader">
    <div class="title">Dashboard</div>
    <div class="userPanel">
      <a href="<?=siteUrl('?logout=1')?>"><i class="fa fa-sign-out-alt logout"></i></a>
      
    <span class="username"><?=$user->name ?? 'Unkuown' ?></span>
    <img src="<?= $user->image?>" width="40" height="40"/></div>
  </div>
  <div class="main">
    <div class="nav">
      <div class="searchbox">
        <div><i class="fa fa-search"></i>
          <input type="search" placeholder="Search"/>
        </div>
      </div>
      <div class="menu">
        <div class="title">Foldres</div>
        <ul class="folder-list">
        <li class=" <?= (isset($_GET['folder_id']))? '' : 'active'?>">
         <a href="<?= siteUrl()?>"><i class="fa fa-folder"></i>ALL</a>
        </li>

          <?php foreach($folders as $folder): ?>
          <li class="<?= ($_GET['folder_id']==$folder->id)?'active': ''?>"> 
            <a href="<?= siteUrl("?folder_id=$folder->id")?>"><i class="fa fa-folder"></i><?= $folder->name ?></a>
            <a href="<?= siteUrl("?delete_folder=$folder->id")?>" onclick="return confirm('Are You Sure to delete this Item?\n<?= $folder->name ?>')"><i class="fa fa-trash del"></i></a>
          </li>
          <?php endforeach; ?>
       
          
        </ul>
      </div>
      <div >
          <input type="text" id="addFolderInput" class="input-folder-task" placeholder="Add New Folder..."/>
        <button id="addFolderBtn" class="btn-folder-task" > <i class="fa fa-plus"></i> </button>
        </div>
       
  
     
    
    
  
     
     
   
 
    </div>
    <div class="view">
      <div class="viewHeader">
        <div class="title">
          <input type="text" id="taskNameInput" class="input-task" placeholder="Add New Task..."/>
        </div>
        <div class="functions">
          <div class="button active">Add New Task</div>
          <div class="button">Completed</div>
          
        </div>
      </div>
      <div class="content">
        <div class="list">
          <div class="title">Today</div>

          <ul>
          <?php if (sizeof($tasks)): ?>
          <?php foreach($tasks as $task): ?>

            <li class="<?= ($task->is_done)? 'checked': ''?>">
            <i data-taskId="<?= $task->id?>" class="is-done fa <?= ($task->is_done)? 'fa-check-square': 'fa-square'?>"></i>
             <span><?=$task->title?></span>
              <div class="info">
                <span class="created-at">Created At  <?= $task->created_at ?></span>
                <a href="<?=siteUrl("?delete_task=$task->id") ?>" onclick="return confirm('Are You Sure to delete this Item?\n<?= $task->title ?>')"><i class="fa fa-trash del"></i></a>
              </div>
            </li>
        
          <?php endforeach; ?>
          <?php else:?>
           <li>NO Tasks Here...</li>

         <?php endif; ?>
           
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- partial -->
  <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script><script  src="./assets/js/script.js"></script>
  <script>
    $(document).ready(function(){ 
      $(".is-done").click(function(e){
        var tid = $(this).attr("data-taskId");
        $.ajax({
           url:"process/ajaxHandler.php",
           method:"post",
           data:{action:"doneSwitch",taskId:tid},
           success:function(response){
             
              location.reload();             
             
           } 

         });
        


      });
      $("#addFolderBtn").click(function(e){
        var  input = $("input#addFolderInput");
         $.ajax({
           url:"process/ajaxHandler.php",
           method:"post",
           data:{action:"addFolder",folderName:input.val()},
           success:function(response){
             if (response == '1') {
            
               $('<li><a href="#"><i class="fa fa-folder"></i>'+input.val()+'</a><a href="#"><i class="fa fa-trash del"></i></a></li>').appendTo('ul.folder-list');
             }else{
                 alert(response);
             }
           } 

         });
        

      });
        $("#taskNameInput").on('keypress',function(e) {
          var inputTask=$("#taskNameInput");
          if(e.which == 13) {
              $.ajax({
                  url:"process/ajaxHandler.php",
                  method:"post",
                  data:{action:"addTask",folderId:<?= $_GET['folder_id'] ?? 0 ?>,taskTitle:inputTask.val()},
                  success:function(response){
                    if (response == '1') {
                      location.reload();
                    }else{
                        alert(response);
                    }
                  } 

            });
            
          }//if
        });
        $("#taskNameInput").focus();
     
       

    });
  </script>

</body>
</html>
